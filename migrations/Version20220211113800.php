<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220211113800 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE positioning DROP CONSTRAINT fk_2b2a70191be62e47');
        $this->addSql('DROP SEQUENCE mission_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE positioning_id_seq CASCADE');
        $this->addSql('DROP TABLE mission');
        $this->addSql('DROP TABLE positioning');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE SEQUENCE mission_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE positioning_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE mission (id INT NOT NULL, title VARCHAR(255) NOT NULL, description TEXT NOT NULL, id_author INT NOT NULL, publication_date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, start_date DATE NOT NULL, end_date DATE DEFAULT NULL, remuneration DOUBLE PRECISION DEFAULT NULL, id_candidate INT DEFAULT NULL, id_status INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE positioning (id INT NOT NULL, id_mission_id INT NOT NULL, id_canditate INT NOT NULL, positioning_date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, candidacy TEXT NOT NULL, id_status INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_2b2a70191be62e47 ON positioning (id_mission_id)');
        $this->addSql('ALTER TABLE positioning ADD CONSTRAINT fk_2b2a70191be62e47 FOREIGN KEY (id_mission_id) REFERENCES mission (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
